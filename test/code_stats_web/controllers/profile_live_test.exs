defmodule CodeStatsWeb.ProfileLiveTest do
  use CodeStatsWeb.ConnCase

  test "viewing a nonexistent user should not crash", %{conn: conn} do
    conn = get(conn, "/users/foobar")

    assert conn.status == 200
    assert conn.resp_body =~ "HTTP/2 404"

    {:ok, _view, _html} = live(conn)
  end
end
