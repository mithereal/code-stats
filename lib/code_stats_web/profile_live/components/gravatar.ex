defmodule CodeStatsWeb.ProfileLive.Components.Gravatar do
  use CodeStatsWeb, :live_component

  @moduledoc """
  Gravatar element that shows an <img> tag to request a gravatar from the backend.

  Requires:
    @username Username of the user.
  """

  @impl true
  def render(assigns) do
    ~L"""
    <img
      src="<%= Routes.profile_path(@socket, :profile_gravatar, @username) %>"
      alt="<%= @username %>'s gravatar"
      class="avatar"
    />
    """
  end
end
