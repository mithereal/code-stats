defmodule CodeStatsWeb.ProfileLive.DataProviders.Utils do
  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStats.User
  alias CodeStats.User.Pulse

  @spec required_shared_data(MapSet.t(module())) :: MapSet.t(DataProviders.SharedData.data_key())
  def required_shared_data(data_providers) do
    Enum.reduce(data_providers, MapSet.new(), &MapSet.union(&2, &1.required_data()))
  end

  @spec update_providers(
          MapSet.t(module()),
          DataProviders.CommonTypes.provider_data(),
          User.t(),
          Pulse.t(),
          User.Cache.t()
        ) :: DataProviders.CommonTypes.provider_data()
  def update_providers(data_providers, data, user, pulse, cache) do
    Enum.reduce(data_providers, %{}, fn provider, acc ->
      updated_data =
        Map.fetch!(data, provider)
        |> provider.update(user, pulse, cache)

      Map.put(acc, provider, updated_data)
    end)
  end
end
