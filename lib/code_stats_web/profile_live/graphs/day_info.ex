defmodule CodeStatsWeb.ProfileLive.Graphs.DayInfo do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias CodeStatsWeb.ProfileLive.Components
  alias CodeStatsWeb.ViewUtils
  alias Phoenix.LiveView

  @behaviour Graphs.Behaviour

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.DayInfo])

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <%= unless match?(%{ average: nil, top_day: nil, most_focused: nil }, assigns[DataProviders.DayInfo]) do %>
      <%=
        live_component(
          __MODULE__,
          id: :top_flows_graph,
          average: assigns[DataProviders.DayInfo].average,
          top_day: assigns[DataProviders.DayInfo].top_day,
          most_focused: assigns[DataProviders.DayInfo].most_focused
        )
      %>
    <% else %>
      <div class="no-data"></div>
    <% end %>
    """
  end
end
