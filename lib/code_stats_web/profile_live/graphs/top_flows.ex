defmodule CodeStatsWeb.ProfileLive.Graphs.TopFlows do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias CodeStatsWeb.ProfileLive.Components
  alias CodeStatsWeb.ViewUtils
  alias Phoenix.LiveView

  @behaviour Graphs.Behaviour

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.TopFlows])

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <%= unless match?(%{ longest: nil, strongest: nil, most_prolific: nil }, assigns[DataProviders.TopFlows]) do %>
      <%=
        live_component(
          __MODULE__,
          id: :top_flows_graph,
          longest: assigns[DataProviders.TopFlows].longest,
          strongest: assigns[DataProviders.TopFlows].strongest,
          most_prolific: assigns[DataProviders.TopFlows].most_prolific
        )
      %>
    <% else %>
      <div class="no-data"></div>
    <% end %>
    """
  end
end
