defmodule CodeStatsWeb.Components.Ad do
  @moduledoc """
  Function components for displaying ads.
  """

  use Phoenix.Component

  import CodeStats.Utils, only: [get_conf: 1]

  def image(assigns) do
    ~H"""
    <div
      id={@placement_id}
      class={[class()] ++ if @dark, do: ["dark"], else: []}
      data-ea-publisher={get_conf(:ea_publisher)}
      data-ea-type="image"
      data-ea-keywords={Enum.join(@keywords, "|")}
    >
    </div>
    """
  end

  defp class(), do: "bordered"
end
