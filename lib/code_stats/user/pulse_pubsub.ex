defmodule CodeStats.User.Pulse.PubSub do
  @moduledoc """
  System to retrieve live pulse updates for the new pulses of one or all users.
  """

  import CodeStats.Utils.TypedStruct

  alias CodeStats.User

  defmodule Data do
    deftypedstruct(%{
      pulse: User.Pulse.t(),
      coords: CodeStats.Utils.GeoTypes.coord_t() | nil,
      cache: User.Cache.t()
    })
  end

  @spec subscribe(:all | pos_integer()) :: :ok
  def subscribe(type)

  def subscribe(:all) do
    {:ok, _} = Registry.register(__MODULE__, :all, nil)
    :ok
  end

  def subscribe(id) do
    {:ok, _} = Registry.register(__MODULE__, id, nil)
    :ok
  end

  @spec publish(
          User.t(),
          User.Pulse.t(),
          CodeStats.Utils.GeoTypes.coord_t() | nil,
          User.Cache.t()
        ) :: :ok
  def publish(user, pulse, coords, cache) do
    data = {__MODULE__, :pulse, %Data{pulse: pulse, coords: coords, cache: cache}}

    Registry.dispatch(__MODULE__, user.id, fn subscribers ->
      for {pid, _} <- subscribers do
        send(pid, data)
      end
    end)

    Registry.dispatch(__MODULE__, :all, fn subscribers ->
      for {pid, _} <- subscribers do
        send(pid, data)
      end
    end)
  end
end
